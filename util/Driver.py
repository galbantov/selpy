from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time


class Driver:
    def __init__(self, driverType, implicitWait=10):
        if driverType == 'Firefox':
            self.driver = webdriver.Firefox()
        elif driverType == 'Chrome':
            self.driver = webdriver.Chrome()
        elif driverType == 'IE':
            self.driver = webdriver.Ie()
        else:
            raise Exception('Unknown webdriver type')
        self.driver.implicitly_wait(implicitWait)
